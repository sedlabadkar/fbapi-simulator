# README #

### What is this repository for? ###

* Aim of this project is to simulate the RESTapi of facebook, more commonly known as Graph API.

* Projects implements a small subset of the actual Graph API, Page, Post, Profile and FriendList api's are included.

* The project has 3 components: 

1. Server : Server is the actual back end server with all user data. There are 64 instances of this and the clients get divided equally.

2. API listener(fbAPI): fbAPI is the application level listener for the HTTP messages from the client. It also forms HTTP response messages based on server's reply

3. Client : Client is the front end simulator that makes HTTP requests to user the fbAPI's provided by the server. The simulator tries to simulate 1000 users.

* All data resides in the memory. No backend database.

* The API is implemented securely. All posts and Private messages are encrypted using AES and RSA, and can only be decrypted by their intended recipients

### How do I get set up? ###

* Requirements: sbt, Scala

* Each of the subparts of the project have their own build.sbt file and can be executed as a different process using "sbt run" command without any args

* First start the server, then fbAPI and lastly the client.